import PySimpleGUI as sg
import subprocess
import numpy as np

#define layout
layout1 = [[sg.Text('DT Name', size=(20,1)),sg.Input('',key='DT Name (Top Level)')],
           [sg.Text('   Name(s) of Children', size=(20,1)),sg.Input('',key='Children (Level 1)')],
           [sg.Text('      Name(s) of Children', size=(20,1)),sg.Input('',key='Chilren (Level 2)')],
           [sg.Text('Input Signal Path', size=(20,1)),sg.Input('',key='Input Path'),sg.FolderBrowse()],
           [sg.Text('Model Path', size=(20,1)),sg.Input('',key='Model'), sg.FolderBrowse()],
           [sg.Button('Save')]]
layout2=[[sg.Text('DT 1 Input', size=(15,1)),sg.Input('',key='Input1')],
           [sg.Text('DT 2 Input', size=(15,1)),sg.Input('',key='Input2')],
           [sg.Text('DT 3 Input', size=(15,1)),sg.Input('',key='Input3')],
           [sg.Text('DT 4 Input', size=(15,1)),sg.Input('',key='Input4')],
         [sg.Button('Save')]]
layout3= [[sg.Text('DT 1 Output', size=(15,1)),sg.Input('',key='Output1')],
           [sg.Text('DT 2 Output', size=(15,1)),sg.Input('',key='Output2')],
           [sg.Text('DT 3 Output', size=(15,1)),sg.Input('',key='Output3')],
           [sg.Text('DT 4 Output', size=(15,1)),sg.Input('',key='Output4')],
          [sg.Button('Save')]]
#Define Layout with Tabs         
tabgrp = [[sg.TabGroup([[sg.Tab('Digital Twin Hierarchy', layout1, border_width =10, element_justification= 'left'),
                    sg.Tab('Input Monitor', layout2, element_justification= 'left'),
                    sg.Tab('Output Monitor', layout3,element_justification= 'left')]], tab_location='centertop',
                       title_color='White', tab_background_color='Gray',selected_title_color='White',
                       selected_background_color='Black', border_width=5)]]

#Original tabgrp code if want to specify color
#tabgrp = [[sg.TabGroup([[sg.Tab('Digital Twin Hierarchy', layout1, title_color='Red',border_width =10, background_color='Green',
#                                element_justification= 'left'),
#                    sg.Tab('Input Monitor', layout2,title_color='Blue',background_color='Yellow', element_justification= 'left'),
#                    sg.Tab('Output Monitor', layout3,title_color='Black',background_color='Pink',element_justification= 'left')]], tab_location='centertop',
#                       title_color='Red', tab_background_color='Purple',selected_title_color='Green',
#                       selected_background_color='Gray', border_width=5)]]  
        
#Define Window
window =sg.Window("Digital Twins",tabgrp)
#Read  values entered by user
event,values=window.read()
values_list = list(values)

#Dummy data, final demo would have the values dynamically coming in from excel or other path
bearing1 = np.array([0,0.09,0.05,0.03,0,0.04,0.09,0.2,0.25,0.2,0.25,0.2,0.09,0.05])
bearing2 = np.array([0,0.07,0.15,0.02,0.04,0.2,0.15,0.3,0.2,0.15,0.09,0.08,0.09,0.1])

for i in range(0,bearing1.size):
    if(-0.1<=bearing1[i]<=0.1 or -0.1<=bearing2[i]<=0.1):
        print('bearing system health state: HEALTHY')
        print('bearing 1: ' + str(bearing1[i]))
        print('bearing 2: ' + str(bearing2[i]) + '\n')
    else:
        print('bearing system health state: FAULTY')
        print('bearing 1: ' + str(bearing1[i]))
        print('bearing 2: ' + str(bearing2[i]) + '\n')

#access all the values and if selected add them to a string
#print(values)
#print(values_list)
#print(values_list[0])

