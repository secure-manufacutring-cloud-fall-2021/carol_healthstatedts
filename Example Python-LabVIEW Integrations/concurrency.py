"""
    Demonstrates how to send information from a live Python operation back
    to LabVIEW.
    
    By design, when you use Call.vi to call a Python function, the Toolkit
    will wait until a response is received.  For complex or long-running
    operations, this isn't desireable as (1) we should be able to interact
    with Python while the operation is pending, and (2) we would like some
    feedback during the operation as to what's happening.
    
    This module demonstrates how to use Python queues and threads to build
    programs which are fully two-directional between Python and LabVIEW.  There
    is a long-running operation simulated by the do_something_complicated
    function, which is launched in a new thread and periodically puts status
    information into a queue.  The queue may be examined by LabVIEW at any time
    to determine the progress of the operation.
    
    One thing to keep in mind: the contents of the queue do not have to be
    strings!  A common pattern for numerical functions is to enqueue partial
    results as soon as possible.  
    
    For example, a simulation function which tests 10,000 different cases
    might pass the results for each to the queue as soon as they are ready.
    That way, LabVIEW can begin processing or displaying the numerical results
    before all cases have finished.
    
    This example also shows a simple way to control a long-running operation,
    by using global variable in the module as a "stop" flag.
"""

from threading import Thread
import sys
import time

if sys.version_info[0] == 2:
    from Queue import Queue, Empty
else:
    from queue import Queue, Empty
    xrange = range


# --- Queue/thread infrastructure ---

status_queue = Queue()
    
worker_thread = None
    

def is_running():
    """ True if the worker thread is running, False otherwise """
    if worker_thread is not None and worker_thread.is_alive():
        return True
    return False


def check_queue():
    """ Get the latest item on the queue, or return an empty string """
    try:
        result = status_queue.get_nowait()
        if result is None:
            result = ""
    except Empty:
        result = ""
    return result


# --- Python and LabVIEW interface code ---
    
keep_running = False  # We can set this from LabVIEW to control the behavior
                      # of the loop, and exit early.


def do_something_complicated():
    """ Simulates a long-running Python operation that periodically puts
    information into the queue.
    """
    
    status_queue.put("Starting up...")
    
    for idx in xrange(10):
    
        # See if LabVIEW has requested that we stop.
        if not keep_running:
            status_queue.put("Exiting early...")
            break
            
        # Do work.  In this case, we just sleep for 1 second.
        status_queue.put("Working... {}".format(idx+1))
        time.sleep(1)
        
        
    status_queue.put("Done.")


def launch_complicated_operation():
    """ Called from LabVIEW to start the operation in a parallel thread """
    global worker_thread
    
    worker_thread = Thread(target=do_something_complicated)
    
    # This line is important; the "daemon" flag instructs Python not to wait
    # for the thread to finish before exiting.  We set it to True, so that
    # when we use Close Session from LabVIEW, Python will be sure to exit.
    worker_thread.daemon = True
    
    worker_thread.start()