"""
    This example shows how high-level Python packages can be combined to
    create extraordinary functionality, with a relatively small amount of code.
    
    This module provides a function launch_dashboard, which when called
    starts a Python-powered web server in a separate thread.  Then, data
    can be posted from LabVIEW to Python, and displayed on a Web dashboard.
    The dashboard updates continously and includes statistics about the
    currently displayed signal.
    
    You will need to install the "flask" Python package to run this example.
    Use the Canopy package manager, and search for "flask".
"""
    
import json
import threading
import numpy as np
import os.path as op
from flask import Flask, render_template, send_from_directory



# --- Data management portion -------------------------------------------------

# Defines a Python object that will hold data sent from LabVIEW

class DataStore(object):
    """ In-memory data store, which also computes statistics """
    
    def __init__(self):
        initial = np.zeros((10,), dtype='f')
        self.last_trace = initial
        self.histogram =  initial, initial
        self.power = initial
        self.fftfreq = initial
        
    def receive_signal(self, arr):
        """ Receive a signal from LabVIEW and update attributes """
        self.last_trace = arr
        self.histogram = np.histogram(arr, 20)
        self.power = (np.abs(np.fft.fft(arr))[0:len(arr)//2])**2
        self.fftfreq = np.fft.fftfreq(len(arr))[0:len(arr)//2]
        
    def get_json(self):
        """ Generate JSON data to send to the web dashboard """
        xx = np.arange(len(self.last_trace)).tolist()
        main_plot = { 'x': xx, 'y': self.last_trace.tolist() }
        hist = {'x': self.histogram[1].tolist(), 'y': self.histogram[0].tolist(), 'type': 'bar'}
        power = {'x': self.fftfreq.tolist(), 'y': self.power.tolist(), 'type': 'bar'}
        return json.dumps({'main_plot': main_plot, 'hist_plot': hist, 'power_plot': power})

# This global object holds the last trace posted, and computed statistics
data_store = DataStore()


# --- Web server portion ------------------------------------------------------

name = 'labview-dash'
template_folder = op.abspath(op.join(op.dirname(__file__), 'templates'))
app = Flask(name, template_folder=template_folder)
app.debug = False

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/assets/<path:path>')
def send_js(path):
    print('request for', path)
    return send_from_directory(op.join(template_folder, 'assets'), path)

@app.route('/data')
def send_data():
    return data_store.get_json()


webserver_thread = None

def launch_dashboard():
    """ Call from LabVIEW to start the web dashboard """
    global webserver_thread
    
    # Be careful not to launch more than once
    if webserver_thread is not None:
        return
        
    # Set the thread to "daemon" to allow Python to exit normally
    # when Close Session.vi is called
    webserver_thread = threading.Thread(target=app.run)
    webserver_thread.daemon = True
    webserver_thread.start()
    
    # Give the server some time to start up
    import time
    time.sleep(1)

