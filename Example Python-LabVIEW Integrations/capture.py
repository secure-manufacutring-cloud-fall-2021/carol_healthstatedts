"""
    Demonstrates how to capture printed output during command execution, and
    return it to LabVIEW.
    
    Ordinarily, printed output is directed to the Python console, whether it
    is visibile or not.  This example module shows how to temporatily redirect
    print statements by assigning a new object to replace sys.stdout.
"""

import sys
import contextlib
import random

if sys.version_info[0] == 2:
    from cStringIO import StringIO
else:
    from io import StringIO


@contextlib.contextmanager
def capture_stdout():
    """ Create a Python "context manager" which redirects standard output
    into a file-like buffer, and returns it.
    """
    buf = StringIO()            # Temporary buffer
    old_stdout = sys.stdout     # Preserve the "real" standard output device
    try:
        sys.stdout = buf        # Patch in the temporary buffer
        yield buf               # Pass control back, so the "with" block runs
    finally:
        sys.stdout = old_stdout # Replace the "real" standard output device
        buf.seek(0)             # Rewind the buffer, so the output can be read


def generate_random():
    """ Returns a random number, and prints some text to the console as well.
    """
    print("Generating random number...")
    result = random.random()
    print("    Generated", result)
    return result
    
    
def generate_random_capture():
    """ Returns a random number, along with any output printed to stdout. """
    
    # The context manager will capture standard output and
    # return it in the file-like object "buf".
    with capture_stdout() as buf:
        result = generate_random()
        
    # Read the printed text into a string
    captured_output = buf.read()

    return result, captured_output
    
