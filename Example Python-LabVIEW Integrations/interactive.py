"""
    This module is part of the Interactively Calling Python example.  It
    contains just two objects: a global variable, and a function which makes
    use of it.  You can change the value of the variable and call the function
    on demand during a long-running Python session.
"""

OFFSET = 0
    
def scale_array(x, slope):
    """ Take an array and scale it (essentially just y = mx +b) """
    return x*slope + OFFSET