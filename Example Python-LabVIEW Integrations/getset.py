"""
    This example shows how to interact with module-level global variables
    in Python.
    
    Module-level globals are available for access from LabVIEW, as well as
    inside the functions and classes you define in your module.  Typically
    they are used for configuration purposes, or to make code easier to
    read and maintain by minimizing the use of literals.
"""

import sys

STANDARD_GREETING = "We hope you enjoy using Python from LabVIEW!"

def say_hello(name):
    """ An improved version of "say_hello" from hello.py, which uses module
    level variables for configuration.
    """
    

    print("Saying hello...")
    return 'Hello, %s! %s' % (to_text(name), to_text(STANDARD_GREETING))


def to_text(s):
    # On Python 3, we have to be careful to convert strings from LabVIEW
    # into text.
    if sys.version_info[0] > 2:
        return s if isinstance(s, str) else s.decode('ascii')
    return s