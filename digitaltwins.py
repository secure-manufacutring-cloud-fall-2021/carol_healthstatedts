import threading
import queue
import sys
import time

class DigitalTwin:
    #Initialize digital twin
    def __init__(self, name, requirement, contextSignals, healthSignals):
        #User specified
        self.name = name;
        self.requirement = requirement;

        #Retrieve initial input signals from LabVIEW
        self.contextSignals = contextSignals;
        self.healthSignals = healthSignals;

    #Queue input signals
    #This section is a work in progress, would need to test and debug with LabVIEW
    def queueInputSignals():
        healthSignalsQueue = queue.Queue();
        contextSignalsQueue = queue.Queue();

        while(True):
            healthSignalsQueue.get()
            contextSignalsQueue.get()
            
            healthSignalsQueue.put()
            contextSignalsQueue.put()
            
    
    def getHealthState():
        healthyMin = min(requirement)
        healthyMax = max(requirement)
        healthyRange = range(healthyMin, healthyMax, 1)
        
        if healthSignals in healthyRange:
            print("System is healthy.")
            
        else:
             print("System is faulty.")   

    def getConfidencePercentage():
        

class ChildDT(DigitalTwin): #child class must be defined with parent in parentheses
    #Instances of child classes inherit all of the attributes and methods of the parent class:
    pass
        
        
class Model:
    def __init__(modelName, modelFilePath):
        self.modelName = modelName;
        self.modelLocation = modelLocation;

    #Depends on the requirement we are modeling...should this be user inputted/set up beforehand?
    #To what extent do these requirements change? Is it always tracking specific parameters and if the parameter is within a certain range, we deem the system healthy/unhealthy?
    def evaluateHealthState():
        

    def outputHealthState():
        

    def evaluateConfidencePercentage():
        

    def outputConfidencePercentage():


class ModelManager:
    #Initialize model manager with list of models
    def __init__(modelList):
        

    def queryModel():
        

    def combineModelOutputs():
